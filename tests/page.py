#!/usr/bin/python3
from time import sleep

from element import Element
from locators import MainPagelocators
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait


class MainPage(object):
    """ Page action methods"""
    
    def __init__(self, driver):
        self.driver = driver
    
    
    def assert_title_matches(self):
        return "Scrive" in self.driver.title


    def sign_document(self, name):
        # Fill Fill Name field
        Element.insert_text(self.driver, MainPagelocators.FULL_NAME, 'Full Name')
        # Click Next
        Element.click(self.driver, MainPagelocators.NEXT_BUTTON)
        # Take a screenshot of modal dialog
        modal = Element.get_element(self.driver, MainPagelocators.MODAL)
        sleep(1)
        modal.screenshot('screenshot.png')
        # Click Sign Im
        Element.click(self.driver, MainPagelocators.SIGN_IN_BUTTON)
        
        
    def assert_document_signed(self, verification_string):
        # Verify the string once the page 2 is loaded
        Element.wait_for_element_to_display(self.driver, MainPagelocators.VERIFICATION_IMAGE)
        document_signed = Element.get_element(self.driver, MainPagelocators.DOCUMENT_SIGNED) 
        assert document_signed.text == verification_string
