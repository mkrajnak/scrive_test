#!/usr/bin/python3
from time import sleep

from selenium.common.exceptions import NoSuchElementException


class Element(object):
    """ Element provides static method for better work and action 
        execution on webelements.
    """  

    @staticmethod
    def wait_for_element_to_display(driver, element_id, timeout=10):
        """ Wait for the visibilit of element.

        Parameters:
            driver: webdriver instance.
            element_id: A tuple from locators.py, contains (By.method, search string).
            timeout: An acceptable amount of time to wait for an element to load. 

        Returns:
            True once an element is found.

        Raises:
            AssertionError if element is not found.
        """
        for _ in range(timeout):
            try:
                element = driver.find_element(*element_id)
                assert element.is_displayed()
                return True
            except NoSuchElementException:
                sleep(1)
                continue
        raise AssertionError('Failed to find {element} after {timeout} retries')

    @staticmethod
    def get_element(driver, element_id):
        """ Make sure that element is loaded and return an element instance.

        Parameters:
            driver: webdriver instance.
            element_id: A tuple from locators.py, contains (By.method, search string).

        Returns:
            :py:class: `selenium.webdriver.remote.webelement.WebElement`
        """
        Element.wait_for_element_to_display(driver, element_id)
        return driver.find_element(*element_id)
    
    @staticmethod
    def click(driver, element_id):
        """ Make sure that element is loaded and click.
        
        Parameters:
            driver: webdriver instance.
            element_id: A tuple from locators.py, contains (By.method, search string).
        """
        Element.get_element(driver, element_id).click()

    @staticmethod
    def insert_text(driver, element_id, text):
        """ Make sure that element is loaded and insert text.
        
        Parameters:
            driver: webdriver instance.
            element_id: A tuple from locators.py, contains (By.method, search string).
        """
        Element.get_element(driver, element_id).send_keys(text)
