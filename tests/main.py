#!/usr/bin/python3
import unittest
from os import getenv

from page import MainPage
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager
from webdriver_manager.firefox import GeckoDriverManager

TEST_URL = "https://staging.scrive.com/t/9221714692410699950/7348c782641060a9"


class ScriveTestCases(unittest.TestCase):
    """ Setup and run Scrive test cases """
    
    def setUp(self):
        # Firefox is a default browse for running test suite
        if getenv('BROWSER') == None or getenv('BROWSER') == 'Firefox':
            self.driver = webdriver.Firefox(service=Service(GeckoDriverManager().install()))
        elif getenv('BROWSER') == 'Chrome':
            self.driver = webdriver.Chrome(service=Service(ChromeDriverManager().install()))
        else:
            raise ValueError('Uknown $BROWSER please set Chrome or Firefox')

        self.driver.get(TEST_URL)

    def test_sign_document(self):
        """ Check page title, fill a name and sign document. """
        page = MainPage(self.driver)
        assert page.assert_title_matches()

        page.sign_document("Full Name")
        page.assert_document_signed("Document signed!")

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
