#!/usr/bin/python3
from selenium.webdriver.common.by import By

class MainPagelocators(object):
    """ Contains a list of IDs that are required to located test elements. """ 
    FULL_NAME = (By.ID, 'name')
    NEXT_BUTTON = (By.XPATH, "/html/body/div/div/div[3]/div[4]/div/a[1]/div/span")
    MODAL = (By.XPATH, "/html/body/div/div/div[3]/div[4]")
    SIGN_IN_BUTTON = (By.XPATH, "/html/body/div/div/div[3]/div[4]/div[1]/a[1]/div")
    VERIFICATION_IMAGE = (By.XPATH, "//*[@id='page2']/img")
    DOCUMENT_SIGNED = (By.XPATH, "/html/body/div/div/div[3]/div[2]/div[2]/div/div[1]/h1/span")