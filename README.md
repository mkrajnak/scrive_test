# Setup

```
$ python3 -m venv .venv
$ source .venv/bin/activate
$ pip install -r requirements.txt
$ cd tests
$ python3 main.py

====== WebDriver manager ======
Current firefox version is 91.6
Get LATEST geckodriver version for 91.6 firefox
Driver [/home/mkrajnak/.wdm/drivers/geckodriver/linux64/v0.30.0/geckodriver] found in cache
.
----------------------------------------------------------------------
Ran 1 test in 11.087s
$ pytest -v main.py --html=pytest_report.html --self-contained-html
======================================== test session starts =========================================
platform linux -- Python 3.9.10, pytest-7.1.1, pluggy-1.0.0 -- /usr/bin/python3
cachedir: .pytest_cache
metadata: {'Python': '3.9.10', 'Platform': 'Linux-5.14.0-65.kpq0.el9.x86_64-x86_64-with-glibc2.34', 'Packages': {'pytest': '7.1.1', 'py': '1.11.0', 'pluggy': '1.0.0'}, 'Plugins': {'metadata': '1.11.0', 'html': '3.1.1'}}
rootdir: /home/mkrajnak/Projects/scrive_test
plugins: metadata-1.11.0, html-3.1.1
collected 0 items                                                                                    

--------- generated html file: file:///home/mkrajnak/Projects/scrive_test/pytest_report.html ---------
======================================= no tests ran in 0.01s ========================================
➜  scrive_test git:(main) ✗ pytest -v tests/main.py --html=pytest_report.html --self-contained-html 
======================================== test session starts =========================================
platform linux -- Python 3.9.10, pytest-7.1.1, pluggy-1.0.0 -- /usr/bin/python3
cachedir: .pytest_cache
metadata: {'Python': '3.9.10', 'Platform': 'Linux-5.14.0-65.kpq0.el9.x86_64-x86_64-with-glibc2.34', 'Packages': {'pytest': '7.1.1', 'py': '1.11.0', 'pluggy': '1.0.0'}, 'Plugins': {'metadata': '1.11.0', 'html': '3.1.1'}}
rootdir: /home/mkrajnak/Projects/scrive_test
plugins: metadata-1.11.0, html-3.1.1
collected 1 item                                                                                     

tests/main.py::ScriveTestCases::test_sign_document PASSED                                      [100%]

--------- generated html file: file:///home/mkrajnak/Projects/scrive_test/pytest_report.html ---------

# Use env variables to change browsers (default is Firefox)
$ BROWSER='Chrome' python3 main.py
====== WebDriver manager ======
Current google-chrome version is 98.0.4758
Get LATEST chromedriver version for 98.0.4758 google-chrome
Driver [/home/mkrajnak/.wdm/drivers/chromedriver/linux64/98.0.4758.102/chromedriver] found in cache
.
----------------------------------------------------------------------
Ran 1 test in 9.831s

OK
```